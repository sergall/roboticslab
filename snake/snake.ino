#include "LedControl.h"
#define joyX A0
#define joyY A1
class Snake {
private:
    //direction
    int vx;
    int vy;
    //snake body positions
    int snakeX[64];
    int snakeY[64];
    int snakeLen;
    int head;
    LedControl *lc;

    int rotateX(int x, int y, int r) {
        if (r == 0) {
            return x;
        }

        if (r == 1) {
            return y;
        }
        return -y;
    }

    int rotateY(int x, int y, int r) {
        if (r == 0) {
            return y;
        }

        if (r == 1) {
            return -x;
        }
        return x;
    }
public:
    Snake(LedControl *lc) {
        reset();
        this->lc = lc;
    }

    void rotate(int r) {
        int oldVx = vx;
        int oldVy = vy;
        vx = rotateX(oldVx, oldVy, r);
        vy = rotateY(oldVx, oldVy, r);
    }

    void move() {
        int nextHead = head + 1;
        nextHead = nextHead % snakeLen;

        lc->setLed(0, snakeX[nextHead], snakeY[nextHead], false);

        int headX = snakeX[head] + vx;
        int headY = snakeY[head] + vy;

        if (headX == 8) {
            headX = 0;
        }
        if (headX == -1) {
            headX = 7;
        }

        if (headY == 8) {
            headY = 0;
        }
        if (headY == -1) {
            headY = 7;
        }

        snakeX[nextHead] = headX;
        snakeY[nextHead] = headY;

        head = nextHead;
        lc->setLed(0, headX, headY, true);
    }

    bool eatSelf() {
        int headX = snakeX[head];
        int headY = snakeY[head];

        for (int i = 0; i < snakeLen; i++) {
            if (i != head && snakeX[i] == headX && snakeY[i] == headY) {
                return true;
            }
        }
        return false;
    }

    bool intersects(int x, int y) {
        for (int i = 0; i < snakeLen; i++) {
            if (snakeX[i] == x && snakeY[i] == y) {
                return true;
            }
        }

        return false;
    }

    void grow() {
        snakeLen++;

        for (int i = snakeLen - 1; i > head; i--) {
            snakeX[i] = snakeX[i - 1];
            snakeY[i] = snakeY[i - 1];
        }
        snakeX[head + 1] = -1;
        snakeY[head + 1] = -1;
    }

    int getHeadX() {
        return snakeX[head];
    }

    int getHeadY() {
        return snakeY[head];
    }

    void reset() {
        //direction and control
        vx = 0;
        vy = 1;
        //snake body
        snakeLen = 3;
        head = 0;
        for (int i = 0; i < 64; i++) {
            snakeX[i] = 0;
            snakeY[i] = 0;
        }

        snakeX[0] = 2;
        snakeY[0] = 4;
        snakeX[1] = 2;
        snakeY[1] = 3;
        snakeX[2] = 2;
        snakeY[2] = 2;
    }
};

class LoopTimer {
private:
    unsigned long previousMillis;
    long interval;
    void (*func)();

public:
    LoopTimer(long interval, void (*func)()) {
        this->previousMillis = millis();
        this->interval = interval;
        this->func = func;
    }

    void inloop() {
        unsigned long currentMillis = millis();

        if (currentMillis - previousMillis >= interval) {
            previousMillis = currentMillis;
            func();
        }
    }
};
//output display
LedControl lc = LedControl(12, 11, 10, 1);
//control
int r;
bool changed;

int buzzerPin = 7;

//animations
bool showFood = false;

//food position
int foodX;
int foodY;

//snake
Snake snake = Snake(&lc);

LoopTimer moveTimer = LoopTimer(200, &gameLoop);

void setup() {
    lc.shutdown(0, false);
    lc.setIntensity(0, 7);

    //random needs for food positioning
    randomSeed(analogRead(3));
    pinMode(buzzerPin, OUTPUT);
    noTone(buzzerPin); 
    startGame();
}


void loop() {
    readJoystik();
    moveTimer.inloop();
}

void gameLoop() {
    if (snake.eatSelf()) {
        startGame();
        return;
    }


    if (snake.getHeadX() == foodX && snake.getHeadY() == foodY) {
      tone(buzzerPin, 700, 100);
      lc.setLed(0, foodX, foodY, true);
      snake.grow();

      moveSnake();
      genFood();
    }
    else {
        moveSnake();
    }

    showFood = !showFood;
    lc.setLed(0, foodX, foodY, showFood);
}

void moveSnake() {
    if (changed) {
        snake.rotate(r);
    }
    changed = false;

    snake.move();
}

void startGame() {
    r = 0;
    changed = false;
    snake.reset();


    //display and animation
    lc.clearDisplay(0);
    showFood = false;

    //generate start food position
    genFood();
}

void genFood() {
    while (true) {
        foodX = random(8);
        foodY = random(8);

        if (!snake.intersects(foodX, foodY)) {
            return;
        }
    }
}

void readJoystik() {
    int xValue = analogRead(joyX);
    int xMap = xValue / 128;

    int newr = 0;
    if (xMap > 5) {
        newr = 1;
    }
    else
        if (xMap < 2) {
            newr = -1;
        }
    if (!changed) {
        changed = newr != r;
        r = newr;
    }
}
