#include <Wire.h>
#include <Servo.h>

class LoopTimer {
  private:
    unsigned long previousMillis;
    long interval;
    void (*func)();

  public:
    LoopTimer(long interval, void (*func)()) {
      this->previousMillis = millis();
      this->interval = interval;
      this->func = func;
    }

    void inloop(unsigned long currentMillis) {

      if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;
        func();
      }
    }
};

const int MPU = 0x68
float GyroX, GyroY, GyroZ;
double yaw;
double GyroErrorZ;
double elapsedTime, currentTime, previousTime;

LoopTimer showTimer = LoopTimer(200, &showLoop);
Servo myservo;

void setup() {
  Serial.begin(19200);
  Wire.begin(22, 21, 100000);
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);
  Wire.write(0x00);
  Wire.endTransmission(true);
  delay(20);
  calculate_error();
  delay(20);
  myservo.attach(6);
}

void loop() {
  currentTime = millis();
  Wire.beginTransmission(MPU);
  Wire.write(0x43);
  Wire.endTransmission(true);
  Wire.requestFrom(MPU, 6, true);
  GyroX = (Wire.read() << 8 | Wire.read()) / 131.0;
  GyroY = (Wire.read() << 8 | Wire.read()) / 131.0;

  int r1 = convertByte(byte(Wire.read()));
  int r2 = byte(Wire.read());

  int Gyro = (r1 * 256) | r2;

  GyroZ = float(Gyro) / 131.0 - GyroErrorZ; // GyroErrorZ ~ (-0.8)

  elapsedTime = (currentTime - previousTime);
  previousTime = currentTime;
  if (abs(GyroZ * elapsedTime / 1000.0) >= 0.001) {
    yaw =  yaw + GyroZ * elapsedTime / 1000.0;
  }

  myservo.write(90.0 + yaw);
  showTimer.inloop(currentTime);
}

void showLoop() {
  Serial.println(yaw);
}

void calculate_error() {
  int c = 0;
  while (c < 1000) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43);
    Wire.endTransmission(true);
    Wire.requestFrom(MPU, 6, true);
    GyroX = Wire.read() << 8 | Wire.read();
    GyroY = Wire.read() << 8 | Wire.read();
    int r1 = convertByte(byte(Wire.read()));
    int r2 = byte(Wire.read());
    int Gyro = (r1 * 256) | r2;
    GyroErrorZ = GyroErrorZ + float(Gyro);
    c++;
  }
  GyroErrorZ = GyroErrorZ / 131000.0;
}

int convertByte(byte b) {
  int r1 = 128 - b;
  if (r1 < 0) {
    return r1 + 128;
  } else {
    return r1 - 128;
  }
}
